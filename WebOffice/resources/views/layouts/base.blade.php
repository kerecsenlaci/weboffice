<!doctype html>
<html lang="en">
  <head>
    @include('template._head')
  </head>
  <body>
    @include('template._nav')
    <div class="container-fluid mt-4">
      @yield('content')
    </div>    
  </body>
</html>